import pickle

import pystan

from loading import load_data_dict

def compile_model():
    with open("model.stan", "r") as f:
        model_code = f.read()
    model = pystan.StanModel(model_code=model_code)
    
    with open('./output/model/model.pkl', 'wb') as f:
        pickle.dump(model, f)


def load_model():
    with open('./output/model/model.pkl', 'rb') as f:
        model = pickle.load(f)
    return model


def run_model(data, n_iter, chains, thin):
    model = load_model()
    fit = model.sampling(data=data, iter=n_iter, chains=chains, thin = thin)
    extract = fit.extract(permuted=True) 

    with open("./output/trace/extract.pkl", "wb") as f:
        pickle.dump(extract, f)
    return extract


if __name__ == "__main__":
    data_dict, data = load_data_dict()
    compile_model()
    n_iter, chains, thin = 1000, 5, 5
    extract = run_model(data_dict, n_iter, chains, thin)
