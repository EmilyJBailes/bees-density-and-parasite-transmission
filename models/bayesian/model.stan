data {
    int<lower=1> n_time_periods;
    int<lower=1> n_compartments;


    int<lower=1> n_low_observations;
    int<lower=1> n_low_colonies;
    int<lower=1,upper=5> low_intensity[n_low_observations];
    int low_observation_colony_lookup[n_low_observations];
    int low_observation_time_period_lookup[n_low_observations];
    int low_colony_compartment_lookup[n_low_colonies];
    int low_density;
    int low_compartment_map[n_low_colonies, low_density-1];
    int is_low_colony_inoculated[n_low_colonies];

    int<lower=1> n_high_observations;
    int<lower=1> n_high_colonies;
    int<lower=1,upper=5> high_intensity[n_high_observations];
    int high_observation_colony_lookup[n_high_observations];
    int high_observation_time_period_lookup[n_high_observations];
    int high_colony_compartment_lookup[n_high_colonies];
    int high_density;
    int high_compartment_map[n_high_colonies, high_density-1];
    int is_high_colony_inoculated[n_high_colonies];

    real cutoff_prior;
    real beta_other_colony_prior_sigma;
}
parameters {
    matrix[n_time_periods, n_low_colonies] low_latent_intensity;
    real<lower=0> low_sigma;
    vector[2] low_beta_other_colony;
    real<lower=0> low_sigma_beta;

    matrix[n_time_periods, n_high_colonies] high_latent_intensity;
    real<lower=0> high_sigma;
    vector[2] high_beta_other_colony;
    real<lower=0> high_sigma_beta;

    ordered[4] cutoffs;
}
model {    
    cutoffs ~  cauchy(0, cutoff_prior); 
    low_sigma ~ gamma(100, 100);
    high_sigma ~ gamma(100, 100);

    low_beta_other_colony ~ normal(0, beta_other_colony_prior_sigma);
    high_beta_other_colony ~ normal(0, beta_other_colony_prior_sigma);

    for (j in 1:n_low_colonies){
        low_latent_intensity[1,j] ~ normal(0, 5);
    }
    for (i in 2:n_time_periods){
        for (j in 1:n_low_colonies){
            real current_period_intensity;
            current_period_intensity = low_latent_intensity[i-1,j];
            for (k in 1:(low_density-1)){
                int is_k_inoculated_colony;
                is_k_inoculated_colony = is_low_colony_inoculated[low_compartment_map[j,k]];

                // Non-inoculated colonies get boosted by the inoculated colony
                if (is_low_colony_inoculated[j] == 1){
                     if (is_k_inoculated_colony == 2) {
                         current_period_intensity = current_period_intensity + ((low_beta_other_colony[2] / (low_density - 1)) * low_latent_intensity[i-1, low_compartment_map[j,k]]); 
                    }
                }
                // Inoculated colonies get boosted by the non-inoculated colonies
                if (is_low_colony_inoculated[j] == 2){
                    current_period_intensity = current_period_intensity + ((low_beta_other_colony[1] / (low_density - 1)) * low_latent_intensity[i-1, low_compartment_map[j,k]]); 
                }
            }
            low_latent_intensity[i,j] ~ normal(current_period_intensity, low_sigma);
        }
    }

    for (j in 1:n_high_colonies){
        high_latent_intensity[1,j] ~ normal(0, 5);
    }
    for (i in 2:n_time_periods){
        for (j in 1:n_high_colonies){
            real current_period_intensity;
            current_period_intensity = high_latent_intensity[i-1,j];
            for (k in 1:(high_density-1)){
                int is_k_inoculated_colony;
                is_k_inoculated_colony = is_high_colony_inoculated[high_compartment_map[j,k]];

                // Non-inoculated colonies get boosted by the inoculated colony
                if (is_high_colony_inoculated[j] == 1){
                     if (is_k_inoculated_colony == 2) {
                         current_period_intensity = current_period_intensity + ((high_beta_other_colony[2] / (high_density - 1)) * high_latent_intensity[i-1, high_compartment_map[j,k]]); 
                    }
                }
                // Inoculated colonies get boosted by the non-inoculated colonies
                if (is_high_colony_inoculated[j] == 2){
                    current_period_intensity = current_period_intensity + ((high_beta_other_colony[1] / (high_density - 1)) * high_latent_intensity[i-1, high_compartment_map[j,k]]); 
                }
            }
            high_latent_intensity[i,j] ~ normal(current_period_intensity, high_sigma);
        }
    }

    for (o in 1:n_low_observations){
        low_intensity[o] ~ ordered_logistic(exp(low_latent_intensity[low_observation_time_period_lookup[o], low_observation_colony_lookup[o]] - 3), cutoffs);
    }

    for (o in 1:n_high_observations){
        high_intensity[o] ~ ordered_logistic(exp(high_latent_intensity[high_observation_time_period_lookup[o], high_observation_colony_lookup[o]] - 3), cutoffs);
    }

}

generated quantities {

    int<lower=0> low_intensity_star[n_low_observations];
    int<lower=0> high_intensity_star[n_high_observations];

    int proposal;
    for (o in 1:n_low_observations){
        proposal = ordered_logistic_rng(exp(low_latent_intensity[low_observation_time_period_lookup[o], low_observation_colony_lookup[o]] - 3), cutoffs);
        low_intensity_star[o] = proposal - 1;
    }
    for (o in 1:n_high_observations){
        proposal = ordered_logistic_rng(exp(high_latent_intensity[high_observation_time_period_lookup[o], high_observation_colony_lookup[o]] - 3), cutoffs);
        high_intensity_star[o] = proposal - 1;
    }

}
