import numpy as np
import pandas as pd

def load_data():
    data = pd.read_csv("data.csv", sep=",")
    data["tp"] += 1
    data["colony_id"] = data["col"]
    data["density"] = data["den"]
    data = data.sort_values(["tp", "colony_id"])
    return data

def load_data_dict():
    data = load_data()
    
    data_dict = {"cutoff_prior": 1.5, "beta_other_colony_prior_sigma": 0.4}
    data_dict["n_time_periods"] = len(set(data["tp"]))
    data_dict["n_compartments"] = 3
    
    data = data[data.density == "high"]
    colony_id_lookup = zip(np.unique(data["colony_id"]), range(1, len(set(data.col)) + 1))
    
    for l in colony_id_lookup:
        data.loc[data["colony_id"] == l[0], "colony_id"] = l[1]
    
    data["compartment_id"] = np.unique(data["comp"], return_inverse =1)[1]

    comp_mapping = []
    for col in set(data["colony_id"]):
        own_comp = data[data.colony_id == col]["compartment_id"].iloc[0]
        comp_mapping.append(set(data[(data["compartment_id"] == own_comp) & (data["colony_id"] != col)]["colony_id"]))
    comp_mapping = pd.DataFrame(comp_mapping).values
    
    data_dict.update({
        "n_high_observations": len(data),
        "n_high_colonies": len(set(data["colony_id"])),
        "high_intensity": data["Int_C"] + 1,
        "high_observation_colony_lookup": data["colony_id"],
        "high_observation_time_period_lookup": data["tp"],
        "high_colony_compartment_lookup": data[["colony_id", "compartment_id"]].drop_duplicates()["compartment_id"] + 1,
        "high_compartment_map": comp_mapping,
        "high_density": comp_mapping.shape[1] + 1,
        "is_high_colony_inoculated": [2 if x == "SBPV" else 1 for x in data[["colony_id", "treat"]].drop_duplicates()["treat"]]
    })
    
    data = load_data()
    data = data[data.den == "low"]
    colony_id_lookup = zip(np.unique(data["colony_id"]), range(1, len(set(data.col)) + 1))
    
    for l in colony_id_lookup:
        data.loc[data["colony_id"] == l[0], "colony_id"] = l[1]
    
    data["compartment_id"] = np.unique(data["comp"], return_inverse =1)[1]

    comp_mapping = []
    for col in set(data["colony_id"]):
        own_comp = data[data.colony_id == col]["compartment_id"].iloc[0]
        comp_mapping.append(set(data[(data["compartment_id"] == own_comp) & (data["colony_id"] != col)]["colony_id"]))
    comp_mapping = pd.DataFrame(comp_mapping).values
    
    data_dict.update({
        "n_low_observations": len(data),
        "n_low_colonies": len(set(data["colony_id"])),
        "low_intensity": data["Int_C"] + 1,
        "low_observation_colony_lookup": data["colony_id"],
        "low_observation_time_period_lookup": data["tp"],
        "low_colony_compartment_lookup": data[["colony_id", "compartment_id"]].drop_duplicates()["compartment_id"] + 1,
        "low_compartment_map": comp_mapping,
        "low_density": comp_mapping.shape[1] + 1,
        "is_low_colony_inoculated": [2 if x == "SBPV" else 1 for x in data[["colony_id", "treat"]].drop_duplicates()["treat"]]
    })
    
    return data_dict, load_data()
